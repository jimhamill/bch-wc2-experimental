# Experiments

TODO

## Wallet Usage

Example from Cashonize.

```
// Setup Wallet Connect.
walletConnectService = new WalletConnectService(
	// Project ID.
	'3fd234b8e2cd0e1da4bc08a0011bbf64',
	// Metadata.
	{
		name: 'Cashonize',
		description: 'Cashonize BitcoinCash Web Wallet',
		url: 'cashonize.com/',
		icons: ['https://cashonize.com/images/favicon.ico'],
	},
	// Event Callbacks.
	{
		// Session State Callbacks.
		onSessionsUpdated: vueApp.onSessionsUpdated,
		onSessionProposal: vueApp.onSessionProposal,
		onSessionDelete: () => {},
		onRPCRequest: vueApp.onRPCRequest,
		onError: vueApp.onError,
	},
	// Wallet Callbacks.
	{
		getUnspents: async () => {
			const wallet = await walletClass.named("mywallet");

			const utxos = await wallet.getUtxos();

			const privateKey = await getPrivateKey();

			const lockingBytecode = cashAddressToLockingBytecode(wallet.cashaddr);

			if(typeof lockingBytecode === 'string') {
				throw new Error('Failed to convert CashAddr to Locking Bytecode');
			}

			const transformed = utxos.map((utxo) => {
				let token;

				if(utxo.token) {
					token = {
						amount: BigInt(utxo.token.amount),
						category: hexToBin(utxo.token.tokenId),
					}

					if(utxo.token.capability || utxo.token.commitment) {
						token.nft = {
							capability: utxo.token.capability,
							commitment: hexToBin(utxo.token.commitment),
						}
					}
				}

				return {
					outpointTransactionHash: hexToBin(utxo.txid),
					outpointIndex: utxo.vout,
					lockingBytecode: lockingBytecode.bytecode,
					unlockingBytecode: {
						template: authenticationTemplateP2pkhNonHd,
						valueSatoshis: BigInt(utxo.satoshis),
						script: 'unlock',
						data: {
							keys: {
								privateKeys: {
									key: privateKey,
								},
							},
						},
						token,
					}
				}
			})

			return transformed;
		},

		getSourceOutput: async (outpointTransactionHash, outpointIndex) => {
			const wallet = await walletClass.named("mywallet");

			const transaction = await wallet.provider.getRawTransactionObject(binToHex(outpointTransactionHash));

			const outpoint = transaction.vout[outpointIndex];

			let token;

			if(outpoint.tokenData) {
				token = {
					amount: BigInt(outpoint.tokenData.amount),
					category: hexToBin(outpoint.tokenData.category),
					nft: outpoint.tokenData.nft ? {
						capability: outpoint.tokenData.nft.capability,
						commitment: outpoint.tokenData.nft.commitment ? hexToBin(outpoint.tokenData.nft.commitment) : undefined,
					} : undefined
				}
			}

			const formatted = {
				valueSatoshis: BigInt(Math.round(outpoint.value * 100_000_000)),
				lockingBytecode: hexToBin(outpoint.scriptPubKey.hex),
				token,
			}

			return formatted;
		},

		getChangeTemplate: async () => {
			return {
				template: authenticationTemplateP2pkhNonHd,
				data: {
					keys: {
						privateKeys: {
							key: await getPrivateKey()
						}
					}
				}
			}
		},
	},
	privateKey,
);

// Start Wallet Connect.
await walletConnectService.start();
```

### Client Usage

TODO
