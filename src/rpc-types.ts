import type {
  AuthenticationTemplate,
  CompilerBCH,
  CompilationData,
  Input,
  InputTemplate,
  Output,
  OutputTemplate,
  TransactionCommon,
} from '@bitauth/libauth';

export type CompilationDataBCH = CompilationData<never>;

export type InputTemplateBCH = InputTemplate<
  CompilerBCH,
  false,
  CompilationDataBCH
>;

export type OutputTemplateBCH = OutputTemplate<
  CompilerBCH,
  false,
  CompilationDataBCH
>;

//-----------------------------------------------------------------------------
// Custom Types
//-----------------------------------------------------------------------------

export interface ChangeTemplate {
  template: AuthenticationTemplate;
  data: CompilationDataBCH;
}

export interface SourceOutputs {
  [outpointId: string]: Output;
}

export type Unspent = Input<{
  template: AuthenticationTemplate;
  data: CompilationDataBCH;
  script: string;
  valueSatoshis: bigint;
  token?: {
    amount: bigint;
    category: Uint8Array;
    nft?: {
      capability: 'none' | 'mutable' | 'minting';
      commitment: Uint8Array;
    };
  };
}> & { lockingBytecode: Uint8Array };

//-----------------------------------------------------------------------------
// Wallet Callbacks
//-----------------------------------------------------------------------------

export interface NetworkCallbacks {
  getSourceOutput: (
    outpointTransactionHash: Uint8Array,
    outpointIndex: number
  ) => Promise<Output>;

  // TODO: Consider block-height, etc for better validations.
}

// Network/Wallet Callbacks.
export interface WalletCallbacks {
  getChangeTemplate: () => Promise<ChangeTemplate>;

  getSourceOutput: (
    outpointTransactionHash: Uint8Array,
    outpointIndex: number
  ) => Promise<Output>;

  getUnspents: () => Promise<Array<Unspent>>;
}

//-----------------------------------------------------------------------------
// RPC Payload/Responses
//-----------------------------------------------------------------------------

export interface RpcRequestResponse {
  request: {
    method: string;
    params?: unknown;
  };
  response: unknown;
}

// Method: bch_getBalance_V0
export interface GetBalanceV0 extends RpcRequestResponse {
  request: {
    method: 'bch_getBalance_V0';
  };
  response: {
    [categoryId: string]: bigint;
  };
}

// Method: bch_getChangeOutput_V0
export interface GetChangeLockingBytecodeV0 extends RpcRequestResponse {
  request: {
    method: 'bch_getChangeLockingBytecode_V0';
  };
  response: Uint8Array;
}

// Method: bch_getTokens_V0
export interface GetTokensV0 extends RpcRequestResponse {
  request: {
    method: 'bch_getTokens_V0';
  };
  response: Array<{
    outpointTransactionHash: Uint8Array;
    outpointIndex: number;
    token: {
      category: Uint8Array;
      amount: bigint;
      nft?: {
        commitment: Uint8Array;
        capability: 'none' | 'mutable' | 'minting';
      };
    };
  }>;
}

// Method: bch_signMessage_V0
export interface SignMessageV0 extends RpcRequestResponse {
  request: {
    method: 'bch_signMessage_V0';
    params: {
      message: Uint8Array;
    };
  };
  response: Uint8Array;
}

// Method: bch_signTransaction_V0
export interface SignTransactionV0Data {
  [identifier: string]: Uint8Array;
}

export interface SignTransactionV0InputWalletScoped {
  outpointTransactionHash: Uint8Array;
  outpointIndex: number;
}

export interface SignTransactionV0InputTemplateScoped {
  outpointTransactionHash: Uint8Array;
  outpointIndex: number;
  script: string;
  data?: SignTransactionV0Data;
}

export interface SignTransactionV0OutputWalletScoped {
  valueSatoshis: bigint;
  token?: Output['token'];
}

export interface SignTransactionV0OutputTemplateScoped {
  script: string;
  valueSatoshis: bigint;
  token?: Output['token'];
  data?: SignTransactionV0Data;
}

export interface SignTransactionV0Params {
  transaction: {
    version?: number;
    locktime?: number;
    inputs?: Array<
      SignTransactionV0InputTemplateScoped | SignTransactionV0InputWalletScoped
    >;
    outputs?: Array<
      | SignTransactionV0OutputTemplateScoped
      | SignTransactionV0OutputWalletScoped
    >;
  };
  signerKey?: string;
  userPrompt?: string;
}

export interface SignTransactionV0Response {
  transactionHash: Uint8Array;
  transaction: TransactionCommon<
    Input,
    Output & { resolvedVariables?: { [identifier: string]: Uint8Array } }
  >;
  sourceOutputs: Output<Uint8Array, Uint8Array>[];
}

export interface SignTransactionV0 extends RpcRequestResponse {
  request: {
    method: 'bch_signTransaction_V0';
    params: Array<SignTransactionV0Params>;
  };
  response: Array<SignTransactionV0Response>;
}
