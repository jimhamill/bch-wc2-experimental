import { CashRPC_V0 } from './rpc';
import { IndexedDbStore } from './store.js';
import {
  deriveSandboxKey,
  derivePublicKey,
  stringifyExtendedJson,
  parseExtendedJson,
} from './utils.js';
import {
  SupportedMethods,
  type RequestEvent,
  type EventCallbacks,
  SupportedChains,
  SupportedEvents,
  BchSession,
} from './types.js';

import WalletConnectCore, { Core } from '@walletconnect/core';
import { getSdkError } from '@walletconnect/utils';
import Client, { Web3Wallet, Web3WalletTypes } from '@walletconnect/web3wallet';

import { binToHex, utf8ToBin } from '@bitauth/libauth';
import { WalletCallbacks } from './rpc-types';

export class WalletConnectService {
  // Wallet Connect.
  core: WalletConnectCore;
  web3Wallet!: Client;

  constructor(
    public readonly projectId: string,
    public readonly metadata: Web3WalletTypes.Metadata,
    public readonly eventCallbacks: EventCallbacks,
    public readonly walletCallbacks: WalletCallbacks,
    public readonly privateKey: Uint8Array
  ) {
    // Instantiate a WC Core with our Project ID.
    // NOTE: We use IndexedDB for storage.
    //       We also use the derived key so that each "account" in a wallet can have a unique WC state.
    //       This is so that we can support multiple accounts running WC concurrently in a Wallet.
    this.core = new Core({
      projectId,
      storage: new IndexedDbStore(
        `wc-${binToHex(derivePublicKey(privateKey))}`
      ),
    });
  }

  async start(): Promise<void> {
    // Initialize our WC Core with the provided metadata.
    this.web3Wallet = await Web3Wallet.init({
      core: this.core,
      metadata: this.metadata,
    });

    // Setup our client callbacks.
    this.web3Wallet.on('session_proposal', this.onSessionProposal.bind(this));
    this.web3Wallet.on('session_delete', this.onSessionDelete.bind(this));
    this.web3Wallet.on('session_request', this.onSessionRequest.bind(this));

    // Invoke the callback to let Wallet know that sessions have been updated.
    await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
  }

  getActiveSessions(): Record<string, BchSession> {
    return this.web3Wallet.getActiveSessions() as unknown as Record<
      string,
      BchSession
    >;
  }

  async disconnectSession(topic: string): Promise<void> {
    try {
      // Disconnect the session.
      await this.web3Wallet.disconnectSession({
        topic,
        reason: getSdkError('USER_DISCONNECTED'),
      });
    } catch (error) {
      console.error(error);
    } finally {
      // Invoke the callback to let Wallet know that sessions have been updated.
      await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    }
  }

  //-----------------------------------------------------------------------------
  // Wallet Connect Hooks
  //-----------------------------------------------------------------------------

  async onSessionProposal(
    proposalEvent: Web3WalletTypes.SessionProposal
  ): Promise<void> {
    try {
      // TODO: Check if this proposal is for BCH blockchain.
      if (!proposalEvent.params.requiredNamespaces.bch) {
        throw new Error('requiredNamespaces must contain "bch" key.');
      }

      // Ensure that the user has selected a chain.
      if (!proposalEvent.params.requiredNamespaces.bch.chains?.length) {
        throw new Error(
          'You must pass in a chain to use ("bitcoincash", "bchtest" or "bchreg")'
        );
      }

      // Get the primary chain to use.
      const [chain, network] =
        proposalEvent.params.requiredNamespaces.bch.chains[0].split(':');

      // Ensure that the chain is BCH.
      if (chain !== 'bch') {
        throw new Error(`Chain (${chain}) is not supported: Must be "bch"`);
      }

      // Ensure that the chain is supported.
      if (
        network !== 'bitcoincash' &&
        network !== 'bchtest' &&
        network !== 'bchreg'
      ) {
        throw new Error(
          `Network (${network}) is not supported: Must be "bitcoincash", "bchtest" or "bchreg"`
        );
      }

      // Derive the Sandboxed Private Key.
      const sandboxedPrivateKey = deriveSandboxKey(
        this.privateKey,
        proposalEvent.params.proposer.metadata.url
      );

      // Derive the Sandboxed Public Key.
      const sandboxedPublicKey = derivePublicKey(sandboxedPrivateKey);

      // Define our namespaces.
      const namespaces = {
        bch: {
          chains: SupportedChains,
          methods: SupportedMethods,
          events: SupportedEvents,
          accounts: [`${chain}:${network}:${binToHex(sandboxedPublicKey)}`],
        },
      };

      // Show UI to get approval for the session.
      await this.eventCallbacks.onSessionProposal(proposalEvent);

      // Send a response letting consuming app know the session is approved.
      await this.web3Wallet.approveSession({
        id: proposalEvent.id,
        namespaces: namespaces,
      });

      // Invoke the callback to let Wallet know that sessions have been updated.
      this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    } catch (error: unknown) {
      // Log the error to the console.
      console.error(error);

      // Set a default reason message.
      let message = 'Unhandled error occurred';

      if (error instanceof Error) {
        message = error.message;
      }

      // Send a response letting the consuming app know the session was rejected.
      await this.web3Wallet.rejectSession({
        id: proposalEvent.id,
        reason: {
          code: 5000,
          message,
        },
      });
    }
  }

  async onSessionDelete(
    deleteEvent: Omit<Web3WalletTypes.BaseEventArgs, 'params'>
  ): Promise<void> {
    try {
      // Show UI to indicate session has been deleted.
      await this.eventCallbacks.onSessionDelete();

      console.log(deleteEvent);
    } catch (error: unknown) {
      console.log(error);
    } finally {
      await this.eventCallbacks.onSessionsUpdated(this.getActiveSessions());
    }
  }

  async onSessionRequest(requestEvent: RequestEvent): Promise<void> {
    // Extract parameters from request.
    const { topic, id, params } = requestEvent;

    // Wrap in a try/catch.
    // If anything fails or the request is declined, we want to respond with the reason.
    try {
      // Get the session associated with this topic.
      const session = this.getActiveSessions()[topic] as unknown as BchSession;

      if (!session) {
        throw new Error(
          'Failed to handle request: No session active for the given topic'
        );
      }

      // Derive the Sandboxed Private Key.
      const sandboxedPrivateKey = deriveSandboxKey(
        this.privateKey,
        session.peer.metadata.url
      );

      // Create an RPC instance, passing in the signer key and template used for this session.
      const cashRPCRequest = new CashRPC_V0(
        sandboxedPrivateKey,
        this.walletCallbacks,
        session.requiredNamespaces.bch.allowedTokens,
        session.requiredNamespaces.bch.template
      );

      // Extract the method.
      const method = params.request.method;

      // Check if this RPC method is allowed (throws if it isn't).
      WalletConnectService.checkMethodIsAllowed(session, method);

      // Parse the params from extended JSON into a JS object.
      const parsedParams = parseExtendedJson(params.request.params || '{}');

      // Execute the request.
      const handleRequest = async () => {
        switch (method) {
          case 'wc_authRequest': {
            const message = utf8ToBin(
              JSON.stringify(parsedParams.cacaoPayload)
            );
            const response = await cashRPCRequest.signMessage({ message });
            return response;
          }
          case 'bch_getBalance_V0':
            return await cashRPCRequest.getBalance();
          case 'bch_getChangeLockingBytecode_V0':
            return await cashRPCRequest.getChangeLockingBytecode();
          case 'bch_getTokens_V0':
            return await cashRPCRequest.getTokens();
          case 'bch_signMessage_V0':
            return await cashRPCRequest.signMessage(parsedParams);
          case 'bch_signTransaction_V0':
            return await cashRPCRequest.signTransaction(parsedParams);
          default:
            throw new Error(`Unsupported method called: ${method}`);
        }
      };

      // Execute the request.
      const response = await handleRequest();

      // Request approval for this request.
      await this.eventCallbacks.onRPCRequest(
        session,
        { method, params: parsedParams },
        response
      );

      // Send the response back to the consuming app.
      await this.web3Wallet.respondSessionRequest({
        topic,
        response: {
          id,
          result: stringifyExtendedJson(response),
          jsonrpc: '2.0',
        },
      });
    } catch (error: unknown) {
      // // Log the error to the console.
      console.error(error);

      // Invoke the onError callback.
      if (error instanceof Error) {
        this.eventCallbacks.onError(error.message);
      }

      // Respond with error.
      await this.web3Wallet.respondSessionRequest({
        topic,
        response: {
          id,
          jsonrpc: '2.0',
          error: {
            code: 5001,
            message: 'User rejected',
          },
        },
      });
    }
  }

  //-----------------------------------------------------------------------------
  // Statics
  //-----------------------------------------------------------------------------

  public static checkMethodIsAllowed(session: BchSession, method: string) {
    if (!session.requiredNamespaces.bch.methods.includes(method)) {
      throw new Error('UNAUTHORIZED_METHOD');
    }
  }

  public static async deleteAccount(privateKey: Uint8Array): Promise<void> {
    await IndexedDbStore.deleteDb(`wc-${binToHex(privateKey)}`);
  }
}
