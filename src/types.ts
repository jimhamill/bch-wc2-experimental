import { Web3WalletTypes } from '@walletconnect/web3wallet';
import type { ProposalTypes, SessionTypes } from '@walletconnect/types';
import { RpcRequestResponse } from './rpc-types';
import { AuthenticationTemplate } from '@bitauth/libauth';

export const SupportedChains = ['bch:bchtest', 'bch:bitcoincash', 'bch:bchreg'];

export const SupportedMethods = [
  'wc_authRequest',
  'bch_getBalance_V0',
  'bch_getChangeLockingBytecode_V0',
  'bch_getTokens_V0',
  'bch_signMessage_V0',
  'bch_signTransaction_V0',
];

export const SupportedEvents = ['balancesChanged'];

export interface BchNamespace extends ProposalTypes.BaseRequiredNamespace {
  template: AuthenticationTemplate;
  allowedTokens: Array<string>;
}

export interface BchSession
  extends Omit<SessionTypes.Struct, 'requiredNamespaces'> {
  requiredNamespaces: {
    bch: BchNamespace;
  };
}

export type RequestEvent = Web3WalletTypes.BaseEventArgs<{
  request: {
    method: string;
    /** Params as Extended JSON (i.e. LibAuth's stringify() function) */
    params: string;
  };
}>;

export interface EventCallbacks {
  onSessionsUpdated: (sessions: Record<string, BchSession>) => Promise<void>;

  // These callbacks MUST throw an Error if approval is not granted.
  onSessionProposal: (
    proposalEvent: Web3WalletTypes.SessionProposal
  ) => Promise<Uint8Array>;
  onSessionDelete: () => Promise<void>;

  onRPCRequest: (
    session: BchSession,
    request: RpcRequestResponse['request'],
    response: RpcRequestResponse['response']
  ) => Promise<void>;

  onError: (error: string) => Promise<void>;
}
