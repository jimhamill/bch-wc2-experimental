import type { RpcRequestResponse, SignMessageV0 } from './rpc-types.js';
import { parseExtendedJson } from './utils.js';

import Client from '@walletconnect/sign-client';
import { generateNonce } from '@walletconnect/auth-client';
import type {
  ProposalTypes,
  SignClientTypes,
  SessionTypes,
} from '@walletconnect/types';

import { stringify, secp256k1, sha256, utf8ToBin } from '@bitauth/libauth';
import EventEmitter from 'events';

// NOTE: This imitates the WalletConnectModal V2 interface for convenience.
//       But, it is not the most intelligent design.
export interface DappModal {
  openModal: (payload?: { uri: string }) => void;
  closeModal: () => void;
  subscribeModal: (callback: (state: { open: boolean }) => void) => void;
}

export interface DappModals {
  connect: DappModal;
  request?: DappModal;
}

export class WalletConnectDapp extends EventEmitter {
  client!: Client;
  session!: SessionTypes.Struct;

  constructor(
    public readonly options: SignClientTypes.Options,
    public readonly chain: string,
    public readonly modals: DappModals
  ) {
    super();
  }

  async start(): Promise<void> {
    // Initialize the WC client.
    this.client = await Client.init(this.options);

    // Set event callbacks.
    // ({ topic, params })
    this.client.on('session_update', this.onSessionUpdate.bind(this));
    this.client.on('session_delete', this.onSessionDisconnect.bind(this));
  }

  async connect(
    requiredNamespaces: ProposalTypes.RequiredNamespaces
  ): Promise<boolean> {
    // If client has not been initialized, then initialize it.
    if (!this.client) {
      await this.start();
    }

    // Connect to the WC relay and get a connection URL.
    const { uri, approval } = await this.client.connect({
      requiredNamespaces,
    });

    // If no URI is available, thrown an error.
    if (!uri) {
      throw new Error('No URI available for some reason');
    }

    try {
      // Show the connect modal.
      this.modals.connect.openModal({ uri });

      // Define a promise that waits for either:
      // a) The modal to be cancelled or
      // b) The session to be approved.
      const waitForConnectOrCancel: Promise<boolean> = new Promise(
        async (resolve) => {
          // TODO: Wrap in try/catch to handle shitty WCModalV2 state.
          //       I'm not even sure if you can cancel a request with WC!

          // Subscribe to modal events (open or close).
          this.modals.connect.subscribeModal((state) => {
            try {
              if (!state.open) {
                resolve(false);
              }
            } catch (error: unknown) {}
          });

          approval().then((session) => {
            this.session = session;

            resolve(true);
          });
        }
      );

      // Await session approval from the wallet.
      const didConnect = await waitForConnectOrCancel;

      // If we did not connect (i.e. it was cancelled), return false.
      if (!didConnect) {
        return false;
      }
    } catch (error: unknown) {
      // Re-throw the error.
      throw error;
    } finally {
      // Close the modal, regardless of what happened.
      this.modals.connect.closeModal();
    }

    // Invoke our connection callback.
    await this.onSessionConnect();

    // Return true as we are now connected.
    return true;
  }

  async disconnect(): Promise<void> {
    if (!this.client) {
      return;
    }

    // Disconnect this session.
    await this.client.disconnect({
      topic: this.session.topic,
      reason: {
        code: 1000,
        message: 'Disconnected',
      },
    });

    // Disconnect any remaining sessions.
    // NOTE: We do this because WalletConnect can easily desync.
    //       So, let's make sure we clear up any stagnant sessions.
    this.client.session.getAll().forEach((session) => {
      this.client.session.delete(session.topic, {
        code: 1000,
        message: 'Disconnected',
      });
    });

    this.onSessionDisconnect();
  }

  async request<T extends RpcRequestResponse>(
    method: T['request']['method'],
    params: T['request']['params'] = undefined
  ): Promise<T['response']> {
    if (!this.client) {
      throw new Error('Client is not initialized');
    }

    if (!this.session) {
      throw new Error('Session is not initialized');
    }

    try {
      if (this.modals.request) {
        this.modals.request.openModal();
      }

      const result = await this.client.request<string>({
        chainId: this.chain,
        topic: this.session.topic,
        request: {
          method,
          params: stringify(params),
        },
      });

      return parseExtendedJson(result);
    } catch (error: unknown) {
      throw error;
    } finally {
      if (this.modals.request) {
        this.modals.request.closeModal();
      }
    }
  }

  async authenticate1() {
    const cacaoPayload = {
      aud: this.options.metadata?.url,
      domain: this.options.metadata?.url,
      chainId: 'eip155:1',
      type: 'eip4361',
      nonce: generateNonce(),
    };

    // TODO: Type me.
    const signature = (await this.request('wc_authRequest', {
      requester: this.session.self,
      cacaoPayload,
    })) as SignMessageV0['response'];

    // Retrieve the public key that we acquired when session was established.
    // NOTE: Split format: ${chain}:${network}:pubkey
    const publicKey = utf8ToBin(
      this.session.namespaces.bch?.accounts[0].split(':')[2]
    );

    // Stringify the cacao payload we sent.
    const cacaoPayloadStringified = JSON.stringify(cacaoPayload);

    // Hash it.
    const cacaoPayloadDigest = sha256.hash(utf8ToBin(cacaoPayloadStringified));

    // Verify the signature.
    if (
      !secp256k1.verifySignatureSchnorr(
        signature,
        publicKey,
        cacaoPayloadDigest
      )
    ) {
      throw new Error('Signature verification failed');
    }

    return {
      cacaoPayload,
      publicKey,
      signature,
    };
  }

  async onSessionConnect() {
    this.emit('paired', this.session.topic);
  }

  async onSessionDisconnect() {
    this.emit('disconnected');
  }

  async onSessionUpdate() {
    this.emit('updated');
  }
}
