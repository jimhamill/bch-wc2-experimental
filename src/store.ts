import { IKeyValueStorage } from '@walletconnect/keyvaluestorage';

import { IDBPDatabase, openDB, deleteDB } from 'idb';

export class IndexedDbStore implements IKeyValueStorage {
  public db: Promise<IDBPDatabase>;

  constructor(name: string, public readonly storeName = 'keyval') {
    this.db = openDB(name, 1, {
      upgrade: (db) => {
        db.createObjectStore(storeName);
      },
    });
  }

  async waitForStart() {
    return await this.db;
  }

  async getItem(key: string) {
    return (await this.db).get(this.storeName, key);
  }
  async setItem<T = any>(key: string, value: T) {
    (await this.db).put(this.storeName, value, key);
  }
  async removeItem(key: string) {
    (await this.db).delete(this.storeName, key);
  }
  async clear() {
    (await this.db).clear(this.storeName);
  }
  async getKeys() {
    return (await this.db).getAllKeys<string>(this.storeName) as Promise<
      Array<string>
    >;
  }
  async getEntries() {
    return (await this.db).getAll(this.storeName);
  }

  public static async deleteDb(name: string) {
    await deleteDB(name, {
      blocked() {},
    });
  }
}
